#include <ctype.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

typedef enum {
  NOTHING = 0,
  PAWN = 1,
  KNIGHT = 2,
  BISHOP = 3,
  ROOK = 4,
  QUEEN = 5,
  KING = 6
} Piece;

//static const uint8_t Value[7] = { 0, 1, 3, 4, 6, 9, 100 };

static const char Abbr[7] = { ' ', 'P', 'C', 'B', 'R', 'Q', 'K' };

typedef enum {
  NONE = 0,
  WHITE = 1,
  BLACK = 2
} Color;

typedef struct __attribute__((packed)) {
  Piece p:3;
  Color c:2;
} Field;

typedef Field State[8][8];

typedef struct {
  uint8_t fromX:3;
  uint8_t fromY:3;
  uint8_t toX:3;
  uint8_t toY:3;
} Move;

typedef enum {
  USER_COMPUTER = 0,
  USER_HUMAN = 1
} UserType;

typedef struct {
  UserType type;
  void (*step)(Move *m, State* s, Color *c);
} User;

typedef struct {
  const User* white;
  const User* black;
  State state;
  int stepsNow;
} Game;

void Computer_Step(Move *m, State* s, Color *c);
void Human_Step(Move *m, State* s, Color *c);

static const User User_Computer = {
  USER_COMPUTER,
  Computer_Step
};

static const User User_Human = {
  USER_HUMAN,
  Human_Step
};

void TODO() {
  fputs ("TODO...\n", stderr);
  exit (1);
};

void Computer_Step(Move *m, State* s, Color *c) {
  TODO();
}

void Human_Step(Move *m, State* s, Color *c) {
  TODO();
}

void trim(char* s) {
  for (; s; s++) {
    if ((*s == '\n') || (*s == '\r')) {
      *s = 0;
      break;
    }
  }
}

void State_Init(State* s) {
  static const Piece init[8] = { ROOK, KNIGHT, BISHOP, KING, QUEEN, BISHOP, KNIGHT, ROOK };
  for (int x = 0; x < 8; x++) {
    for (int y = 0; y < 8; y++) {
      if (y < 2) {
        (*s)[x][y].c = WHITE;
      } else if (y < 6) {
        (*s)[x][y].c = NONE;
      } else {
        (*s)[x][y].c = BLACK;
      }

      if ((y==0) || (y==7)) {
        (*s)[x][y].p = init[x];
      } else if ((y == 1) || (y==6)) {
        (*s)[x][y].p = PAWN;
      } else {
        (*s)[x][y].p = NOTHING;
      }
    }
  }
}

void State_Copy(State* from, State* to) {
  memcpy(to, from, sizeof(State));
}

int Game_Ready(Game* s) {
  TODO();
  return 0;
}

void Game_PrintState(Game *g) {
  State *s = &(g->state);
  printf ("  A B C D E F G H\n\n");
  for (int y = 0; y < 8; y++) {
    printf ("%i ", (8 - y));
    for (int x = 0; x < 8; x++) {
      char c = Abbr[(*s)[x][y].p];
      if ((*s)[x][y].c == BLACK) {
        c = c - 'A' + 'a';
      }
      printf ("%c ", c);
    }
    printf (" %i\n\n", (8 - y));
  }
  printf ("  A B C D E F G H\n");
}


void Game_Goodbye(Game *g) {
  TODO();
}

int Move_IsLegal(Move* m, Game* g) {
  TODO();
  return 0;
}

void Move_Read(Move* m, Game* g) {
  char descr[7];
  while (1) {
    printf ("? ");
    bzero(descr, 7);
    fgets(descr, 7, stdin);
    trim(descr);
    if (strlen(descr) != 4) {
      goto BAD_MOVE;
    }
    descr[0] = tolower(descr[0]);
    descr[2] = tolower(descr[2]);
    if ((descr[0] < 'a') || (descr[0] > 'h') || (descr[1] < '1') || (descr[1] > '8') || (descr[2] < 'a') || (descr[2] > 'h') || (descr[3] < '1') || (descr[3] > 'h')) {
      goto BAD_MOVE;
    }
    m->fromX = descr[0] - 'a';
    m->fromY = descr[1] - '1';
    m->toX = descr[2] - 'a';
    m->toY = descr[2] - '1';
    if (!Move_IsLegal(m, g)) {
      goto BAD_MOVE;
    }
    return;

    BAD_MOVE:

    fputs("Illegal move\n", stdout);
  }
}

void Move_Exec(Move* m, Game* g) {
  TODO();
}

void Move_Find(Move* m, Game* g) {
  TODO();
}

void Game_Init(Game* g) {
  g->white = &User_Human;
  g->black = &User_Computer;
  State_Init(&(g->state));
  g->stepsNow = WHITE;
}

void Game_Play(Game *g) {
  while (1) {
    Game_PrintState(g);
    Move m;
    Move_Read(&m, g);
    Move_Exec(&m, g);
    Game_PrintState(g);
    if (Game_Ready(g)) {
      break;
    }
    Move_Find(&m, g);
    Game_PrintState(g);
    if (Game_Ready(g)) {
      break;
    }
  }
  Game_Goodbye(g);
}

int main(int argc, char** argv) {
  Game g;

  printf ("sizeof(Field): %lu sizeof(State): %lu\n\n", sizeof(Field), sizeof(State));

  Game_Init(&g);
  Game_Play(&g);

  return 0;
}
